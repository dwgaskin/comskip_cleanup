comskip cleanup
=========
####A ruby 1.9.3 script to clean up orphaned files when using:
* Windows Media Center (MCE)
* Comskip
* DVRMS Toolbox

### What is it?
*This comskip_cleanup script is a small ruby program that cleans up old/orphaned .log, .edl, .xml, logo files, etc. that are sometimes left behind when using Windows Media Center (MCE), DVRMS toolbox, and Comskip.*
### About
I wanted to clean-up some orphaned files left behind by Comskip and DVRMS toolbox on my MCE media machine, so I whipped-up a quick & dirty script to automate the process.
I can't be the only one with the problem, so I've added a couple of comments to the code and posed it here. Your mileage may vary, and I can't make any promises that it will work-- or make sense-- with your setup. But I *do* hope it proves useful to someone.
This is a quick fix; For those that know Ruby well, I know it's ugly and inelegant! In fact, it started as a script that got the same results in a couple-dozen lines of code!
I've fleshed it out quite a bit so that it's easy to modify and tinker with for beginners trying to get it working on their MCE machine. For example, I've added lots of debug and backup options that you can use to test and customize it.
### Pre-requisites:
#### Ruby:
You'll need ruby 1.9.3 installed to run the script. I haven't tried it on other versions, but I've used the updated hash syntax for a couple of options, so I know it won't work on 1.8.x without some minor modifications. (Changing
`option: value` pairs to `option => value`, would be a start.) And I'm not sure if some of the includes are included in earlier versions of ruby. If it's really important I'll consider making it work on 1.8.3, but I can't imagine why...
### Configuration notes:
* The code is well commented. At the *very* least, be sure to change the paths at the beginning of the script to work with your configuration!
* Note that when you're adjusting paths in Ruby pointing to Windows files, you'll need to remember to use forward-slashes instead of backslashes, as the backslash is used as an escape character in Ruby. So instead of `c:\comskip\xml\location` as you're accustomed to in DOS, use: `c:/comskip/xml/location`.
* It is meant to be run using a .bat file (example included) from Windows task scheduler. As you are testing, you can run it from a command prompt using `ruby clean_comskip.rb`. If you use the .bat file, you'll need to edit it too!