require 'set'
require 'prettyprint'
require 'fileutils'
require 'date'
require 'zip/zip'
# -------------------------------------- #
# ............CONFIGURATION............
# -------------------------------------- #
## PATHS: What folders to check for orphaned files, backup, and clean.
# Everyone needs to customize this to their own system. Don't run the script until you do!
path_to_wtv = "G:/Recorded TV" # Where your WTV recordings are located
path_to_comskip_outputs = "G:/DvrmsToolbox/CommercialsXml" # Folder where DVRMS Toolbox puts the .xml files used by the MCE plugin

## Folder Names: What to call the trash folders.
# For instead of deleting the files outright, this script creates a directory, (named .trash by default), moves
# the orphaned files there, and then deletes the folder. What the folder is called doesn't really matter,
# so you should probably leave this alone.
XML_TRASH_FOLDER_NAME = '.trash'
RECORDINGS_TRASH_FOLDER_NAME = '.trash'

## DEBUG / INFORMATION: How much extra info to print, and tools to help you get started.
NOOP = true # Turns on the `noop: true` option so that file deletes and moves can be tweaked without actually changing things.
            # In other words, nothing will actually be deleted while this is set to `true`, though backup folders will
            # likely be created.
            # # This should be set to true as you're editing the script to get it working. Once it's finding the right files
            # and printing the proper commands to the screen, set it to false so that it can actually delete the orphaned files.

PRINT_TYPES = true  # Print a breakdown of files to be deleted by type. Interesting, so I leave this on.
VERBOSE_DELETE = true # Deletes and moves are printed to the terminal when set to `true`
SHOW_MATCHING_DEBUG = false # Provides some info about what is, and isn't matched. Should be set to false, as it's more info than you need, but
                            # if you're having problems or want to see everything, go ahead and set it to `true`

# ---- BACKUPS: Creates a dated .zip file backup before deleting files. ----------

# 1. The following three options are used to create a a zip backup of unmatched/orphaned files located in the
# folder where the comskip XML output is created:
BACKUP_ORPHANS = true  # This turns the backup on when set to `true`. This should be on if you want backups, and `false`
                       # if you're confidant enough with how this script functions to go "without a net"
  ORPHANS_FOLDER_NAME = "orphan_backups"   # What the backup folder will be called. Just a name, not a path.
  BACKUP_ORPHANS_VERBOSE = false   # Set to true for testing if you're the nervous sort, or want to see what's going on during the backup process.

# 2. Backup (almost) everything in the comskip output directory. Overkill.
# Should be off once you're confidant that the script isn't going to break your setup.
# Backs up the entire folder contents, EXCEPT subdirectories.
CREATE_FULL_FOLDER_BACKUP = false # Turns backup on and off with 'true' or 'false'
  FULL_BACKUP_FOLDER_NAME = "full backups"    # What the backup folder will be called. Just a name, not a path.
  FULL_BACKUP_VERBOSE = false   # Set to true for testing if you're the nervous sort, or want to see what's going on during the backup process.


# 3. Sometimes .log files wind up mixed in with the .WTV recordings made by Media Center.
# This backs up those files, along with other orphaned files, and then deletes them.
BACKUP_RECORDINGS_ORPHANS = true # Turns backup on and off with 'true' or 'false'
  RECORDINGS_ORPHANS_FOLDER_NAME = "orphan_backups"
  RECORDINGS_BAK_ORPHANS_VERBOSE = false    # Set to true for testing

# The following is a regular expression specifying those extensions in the WTV recordings folder that should NEVER
# be considered orphans and deleted.
SKIP_TYPES_REGEX = /\.bat|\.ini|\.db/ # You can add others by inserting an or (|), an escaped dot (\.),
# and an extension (.ext) to the line above, before the '/'. (regular expressions in ruby are contained between /'s, ex: /regex/)
# For example, to add an exemption for all .abc files, it would look like:
# /\.bat|\.ini|\.db|\.abc/

# TODO: Add actual logging to file, real log levels, etc.
# ......... END CONFIGURATION: You probably don't need to mess with anything below this line .......... #

xml_trash_folder = "#{path_to_comskip_outputs}/#{XML_TRASH_FOLDER_NAME}"
recordings_trash_folder = "#{path_to_wtv}/#{RECORDINGS_TRASH_FOLDER_NAME}"

# Data structures for holding filenames of specific types. Think of them like bins that files will be sorted into.
shows = Set.new  # The "bin" where existing WTV recordings go. When files are found that match an existing recording, they will be kept.
xml_matches = Array.new   # Files to keep in the folder where Comskip puts the .xml files it creates.
xml_orphans = Array.new   # Files that don't match current recordings. Aka orphaned/old files that should be cleaned up.
recordings_matches = Array.new  # Files to keep in the Recordings folder
recordings_orphans = Array.new  # Orphaned files in the Recordings folder


# Filling the "shows" bin: Iterate through .wtv files & grab their filenames
wtv_regex = /(.*)\.wtv/
Dir.glob("#{path_to_wtv}/*.{wtv}") {
  |filename|  shows.add(filename.split("/").last.gsub(/\.wtv$/,""))
}
#Dir.chdir(path_to_comskip_outputs) # Depreciated b/c all paths are specified manually.

# Analyze the files CommercialsXML directory. Sort which files match, which are orphaned.
puts "\nAnalyzing #{path_to_comskip_outputs} for orphaned files that need to be cleaned up..."
xml_file_types = Set.new()
xml_extensions_list = Array.new
Dir.foreach(path_to_comskip_outputs).each do |f|
  if shows.include?(f.gsub(/(\.[\w\d]{2,3}){1,2}$/, "")) # The regex checks for both single extension files, and those with
                                                         # two (Ex: file.tmp.log.) and trimms them off so we're just looking at the filename.
    xml_matches << f # Put/sort (full) filenames that match into their respective "bin"
  elsif File.ftype(path_to_comskip_outputs + '/' + f) == "directory"  # Don't mess with directories. Just look at files.
    puts "\tDEBUG: (Removed directory '#{f}' from match)" if SHOW_MATCHING_DEBUG
  else
    xml_orphans << f  # Sort (full) filenames that don't match a recording.wtv in their own "bin."

    # Below, I'm building an array containing an (inclusive) list of all the file extensions of the orphaned files, and a Set that lists each
    # occurrence of an extension just once. This is used for some interesting summary data, but not for actually removing files.
    f.sub(/((?:\.[\w\d]{2,3}){1,2})$/) {|extension|
      xml_file_types << extension
      xml_extensions_list << extension
    } if PRINT_TYPES
  end
end
puts "-> Found #{xml_matches.count} files matching one of #{shows.count} recordings."
puts "-> There appear to be #{xml_orphans.count} orphaned files that can safely be deleted."

# Analyze the Recordings directory for junk files, defined as anything other than WTV, or files with extensions that match
# the SKIP_TYPES_REGEX regular expression specified in the config file.
puts "\nAnalyzing #{path_to_wtv} for orphaned files that need to be cleaned up..."
recordings_file_types = Set.new()
recordings_extensions_list = Array.new
Dir.foreach(path_to_wtv).each do |filename|
  if File.ftype(path_to_wtv + '/' + filename) == "directory"
    puts "\tDEBUG: (Removed directory '#{filename}' from match)" if SHOW_MATCHING_DEBUG
  elsif File.extname(path_to_wtv + '/' + filename).match(SKIP_TYPES_REGEX)
    puts "\tDEBUG: (Ignoring protected filetype '#{filename}' from match)" if SHOW_MATCHING_DEBUG
  elsif shows.include?(filename.gsub(/(\.[\w\d]{2,3}){1,2}$/, ""))
    recordings_matches << filename
  else
    recordings_orphans << filename
    # Build an array containing an (inclusive) list of extensions, and a Set that lists each occurrence once.
    filename.sub(/((?:\.[\w\d]{2,3}){1,2})$/) {|extension|
      recordings_file_types << extension
      recordings_extensions_list << extension
    } if PRINT_TYPES
  end
end
puts "-> Found #{recordings_matches.count - shows.count} files, (not including *.wtv), matching one of #{shows.count} recordings."
puts "-> There appear to be #{recordings_orphans.count} orphaned files that can safely be deleted."


### Print information about ALL of the orphaned files:
if PRINT_TYPES
  # xml folder occurrence:
  if xml_orphans.count > 0
    puts "\nOrphaned file types to be deleted from #{path_to_comskip_outputs}"
    xml_file_types.each { |ext| puts "\t#{xml_extensions_list.count(ext)} files of type \t#{ext.to_s}" }
  end

  # recordings folder occurrence:
  if recordings_orphans.count > 0
    puts "Orphaned file types to be deleted from #{path_to_wtv}"
    recordings_file_types.each { |ext| puts "\t#{recordings_extensions_list.count(ext)} files of type \t#{ext.to_s}" }
  end
end

### Backup and delete XML FOLDER orphaned files
if xml_orphans.count > 0
  # Backup ComskipXML xml_orphans before delete
  if BACKUP_ORPHANS && !NOOP
    FileUtils.mkdir_p("#{path_to_comskip_outputs}/#{ORPHANS_FOLDER_NAME}")
    # puts DateTime.now.strftime('%Y-%m-%d %H_%M_%S')
    zipfile_name = "#{path_to_comskip_outputs}/#{ORPHANS_FOLDER_NAME}/#{DateTime.now.strftime('%Y-%m-%d %H_%M_%S')}.zip"
    # Create zipfile
    puts "\nCreating a backup of #{xml_orphans.count} orphaned files at #{zipfile_name}..."
    Zip::ZipFile.open(zipfile_name, Zip::ZipFile::CREATE) do |zipfile|
      xml_orphans.each_with_index do |filename,i|
        # Two arguments:
        # - The name of the file as it will appear in the archive
        # - The original file, including the path to find it
         puts "adding '#{filename}'" if BACKUP_ORPHANS_VERBOSE
        zipfile.add(filename, path_to_comskip_outputs + '/' + filename)
      end
      print "Done!"
    end
  end

  # Backup the entire ComskipXML folder, both xml_orphans and files to be left alone, before delete. Overkill, but a safer way to test.
  if CREATE_FULL_FOLDER_BACKUP
    FileUtils.mkdir_p("#{path_to_comskip_outputs}/#{FULL_BACKUP_FOLDER_NAME}")
    zipfile_name = "#{path_to_comskip_outputs}/#{FULL_BACKUP_FOLDER_NAME}/full_backup_#{DateTime.now.strftime('%Y-%m-%d %H_%M_%S')}.zip"
    # Create zipfile
    puts "\nCreating a backup of #{Dir.foreach(path_to_comskip_outputs).count} files at #{zipfile_name}..."
    Zip::ZipFile.open(zipfile_name, Zip::ZipFile::CREATE) do |zipfile|
      Dir.foreach(path_to_comskip_outputs) do |filename|
        # Two arguments:
        # - The name of the file as it will appear in the archive
        # - The original file, including the path to find it
        puts "adding '#{filename}'" if FULL_BACKUP_VERBOSE
        zipfile.add(filename, path_to_comskip_outputs + '/' + filename) unless File.ftype(filename) == "directory"
      end
      print "Done!\n"
    end
  end

  ## Do deletions
  # Delete xml_orphans
  xml_orphans.each {|target|
    FileUtils.mkdir_p(xml_trash_folder)
    FileUtils.move("#{path_to_comskip_outputs}/#{target}", "#{xml_trash_folder}/#{target}", verbose: VERBOSE_DELETE, force: true, noop: NOOP) if File.exists? xml_trash_folder
    FileUtils.remove_dir(xml_trash_folder, noop: NOOP)
  } if xml_orphans.count > 0
  puts "Done #{NOOP ? "pretending to move" : "moving"} #{xml_orphans.count} orphaned files into #{xml_trash_folder}."
else
  puts "No xml_orphans found, so nothing to do."
end


###  Backup and delete orphans from RECORDINGS folder
if recordings_orphans.count > 0
  # Backup recordings folder orphans before delete
  if BACKUP_ORPHANS #&& !NOOP
    FileUtils.mkdir_p("#{path_to_wtv}/#{RECORDINGS_ORPHANS_FOLDER_NAME}")
    zipfile_name = "#{path_to_wtv}/#{RECORDINGS_ORPHANS_FOLDER_NAME}/#{DateTime.now.strftime('%Y-%m-%d %H_%M_%S')}.zip"
    # Create zipfile
    puts "\nCreating a backup of #{recordings_orphans.count} orphaned files at #{zipfile_name}..."
    Zip::ZipFile.open(zipfile_name, Zip::ZipFile::CREATE) do |zipfile|
      recordings_orphans.each_with_index do |filename,i|
        # Two arguments:
        # - The name of the file as it will appear in the archive
        # - The original file, including the path to find it
        puts "adding '#{filename}'" if RECORDINGS_BAK_ORPHANS_VERBOSE
        zipfile.add(filename, path_to_wtv + '/' + filename)
      end
      puts "Done!"
    end
  end

  ## Do deletions
  # Delete xml_orphans
 recordings_orphans.each {|target|
    FileUtils.mkdir_p(recordings_trash_folder)
    FileUtils.move("#{path_to_wtv}/#{target}", "#{recordings_trash_folder}/#{target}", verbose: VERBOSE_DELETE, force: true, noop: NOOP) if File.exists? recordings_trash_folder
    FileUtils.remove_dir(recordings_trash_folder, noop: NOOP)
  }
  puts "Done #{NOOP ? "pretending to move" : "moving"} #{recordings_orphans.count} orphaned files into #{recordings_trash_folder}."
else
  puts "No recordings orphans found, so nothing to do."
end


